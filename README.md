# Simple Paint

**Progresso:** CONCLUÍDO<br />
**Autor:** Paulo Victor de Oliveira Leal<br />
**Data:** 2015<br />

### Objetivo
Implementar uma versão simples do paint utilizando os algoritmos DDA e Bresenham

### Observação

IDE:  [Visual Studio 2017](https://visualstudio.microsoft.com/)<br />
Linguagem: [C#](https://docs.microsoft.com/pt-br/dotnet/framework/winforms/)<br />
Banco de dados: Não utiliza<br />

### Execução

No Visual Studio
    

### Contribuição

Esse projeto está concluído e é livre para uso

## Licença
<!---

[//]: <> (The Laravel framework is open-sourced software licensed under the [MIT license]https://opensource.org/licenses/MIT)

-->