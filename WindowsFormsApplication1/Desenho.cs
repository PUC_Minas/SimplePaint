﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace SimplePaint {
    public class Desenho : IDisposable {

        bool disposed;

        protected virtual void Dispose( bool disposing ) {
            if( !disposed ) {
                if( disposing ) {
                    //dispose managed resources
                }
            }
            //dispose unmanaged resources
            disposed = true;
        }

        public Bitmap bmap { get; set; }
        public bool setado { get; set; }
        public Point down { get; set; }
        public Point up { get; set; }
        public bool algoritmo { get; set; }

        public void Dispose( ) {
            Dispose( true );
            GC.SuppressFinalize( this );
        }

        public override string ToString( ) {
            return "Camada";
        }
    }
}
