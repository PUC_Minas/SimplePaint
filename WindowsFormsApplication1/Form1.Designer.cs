﻿namespace SimplePaint
{
    partial class tela
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.imagem = new System.Windows.Forms.PictureBox();
            this.painel = new System.Windows.Forms.Panel();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label11 = new System.Windows.Forms.Label();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.camadas_list = new System.Windows.Forms.CheckedListBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.btApagar = new System.Windows.Forms.Button();
            this.desenhar = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBoxAlgoritmo = new System.Windows.Forms.CheckedListBox();
            this.btCor = new System.Windows.Forms.Button();
            this.txtY = new System.Windows.Forms.TextBox();
            this.txtX = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cdlg = new System.Windows.Forms.ColorDialog();
            ((System.ComponentModel.ISupportInitialize)(this.imagem)).BeginInit();
            this.painel.SuspendLayout();
            this.SuspendLayout();
            // 
            // imagem
            // 
            this.imagem.BackColor = System.Drawing.Color.White;
            this.imagem.Dock = System.Windows.Forms.DockStyle.Fill;
            this.imagem.Location = new System.Drawing.Point(0, 0);
            this.imagem.Name = "imagem";
            this.imagem.Size = new System.Drawing.Size(1348, 697);
            this.imagem.TabIndex = 0;
            this.imagem.TabStop = false;
            this.imagem.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.imagem.MouseMove += new System.Windows.Forms.MouseEventHandler(this.imagem_MouseMove);
            this.imagem.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // painel
            // 
            this.painel.Controls.Add(this.textBox5);
            this.painel.Controls.Add(this.button1);
            this.painel.Controls.Add(this.label15);
            this.painel.Controls.Add(this.textBox3);
            this.painel.Controls.Add(this.label16);
            this.painel.Controls.Add(this.button3);
            this.painel.Controls.Add(this.label13);
            this.painel.Controls.Add(this.textBox4);
            this.painel.Controls.Add(this.label14);
            this.painel.Controls.Add(this.button2);
            this.painel.Controls.Add(this.label11);
            this.painel.Controls.Add(this.textBox1);
            this.painel.Controls.Add(this.textBox2);
            this.painel.Controls.Add(this.label12);
            this.painel.Controls.Add(this.camadas_list);
            this.painel.Controls.Add(this.label3);
            this.painel.Controls.Add(this.label7);
            this.painel.Controls.Add(this.label9);
            this.painel.Controls.Add(this.label8);
            this.painel.Controls.Add(this.label6);
            this.painel.Controls.Add(this.btApagar);
            this.painel.Controls.Add(this.desenhar);
            this.painel.Controls.Add(this.label2);
            this.painel.Controls.Add(this.label1);
            this.painel.Controls.Add(this.checkBoxAlgoritmo);
            this.painel.Controls.Add(this.btCor);
            this.painel.Controls.Add(this.txtY);
            this.painel.Controls.Add(this.txtX);
            this.painel.Controls.Add(this.label4);
            this.painel.Controls.Add(this.label10);
            this.painel.Controls.Add(this.label5);
            this.painel.Dock = System.Windows.Forms.DockStyle.Right;
            this.painel.Location = new System.Drawing.Point(1206, 0);
            this.painel.Name = "painel";
            this.painel.Size = new System.Drawing.Size(142, 697);
            this.painel.TabIndex = 1;
            // 
            // textBox5
            // 
            this.textBox5.Location = new System.Drawing.Point(46, 382);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(24, 20);
            this.textBox5.TabIndex = 39;
            this.textBox5.Text = "y";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(76, 380);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(45, 23);
            this.button1.TabIndex = 35;
            this.button1.Text = "OK";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(9, 366);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(42, 13);
            this.label15.TabIndex = 37;
            this.label15.Text = "Escala:";
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(12, 382);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(28, 20);
            this.textBox3.TabIndex = 36;
            this.textBox3.Text = "x";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1, 401);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(139, 13);
            this.label16.TabIndex = 38;
            this.label16.Text = "______________________";
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(76, 322);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(45, 23);
            this.button3.TabIndex = 30;
            this.button3.Text = "OK";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click_1);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(9, 308);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(51, 13);
            this.label13.TabIndex = 33;
            this.label13.Text = "Rotação:";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(12, 324);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(60, 20);
            this.textBox4.TabIndex = 31;
            this.textBox4.Text = "Graus";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(1, 343);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(139, 13);
            this.label14.TabIndex = 34;
            this.label14.Text = "______________________";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(76, 267);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(45, 23);
            this.button2.TabIndex = 25;
            this.button2.Text = "OK";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(9, 238);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(119, 26);
            this.label11.TabIndex = 28;
            this.label11.Text = "Translação: \r\n(Fatores de Translação)";
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(46, 269);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(24, 20);
            this.textBox1.TabIndex = 27;
            this.textBox1.Text = "y";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(12, 269);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(28, 20);
            this.textBox2.TabIndex = 26;
            this.textBox2.Text = "x";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(1, 288);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(139, 13);
            this.label12.TabIndex = 29;
            this.label12.Text = "______________________";
            // 
            // camadas_list
            // 
            this.camadas_list.FormattingEnabled = true;
            this.camadas_list.Location = new System.Drawing.Point(12, 445);
            this.camadas_list.Name = "camadas_list";
            this.camadas_list.Size = new System.Drawing.Size(120, 184);
            this.camadas_list.TabIndex = 22;
            this.camadas_list.SelectedIndexChanged += new System.EventHandler(this.camadas_list_SelectedIndexChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 428);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(54, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Camadas:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(12, 646);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(118, 26);
            this.label7.TabIndex = 17;
            this.label7.Text = "Software desenvolvido \r\n     por Paulo Leal";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(2, 88);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(139, 13);
            this.label9.TabIndex = 20;
            this.label9.Text = "______________________";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(23, 675);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "p_leal@ymail.com";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(1, 629);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 13);
            this.label6.TabIndex = 16;
            this.label6.Text = "______________________";
            // 
            // btApagar
            // 
            this.btApagar.Location = new System.Drawing.Point(26, 106);
            this.btApagar.Name = "btApagar";
            this.btApagar.Size = new System.Drawing.Size(75, 23);
            this.btApagar.TabIndex = 6;
            this.btApagar.Text = "Limpar Tela";
            this.btApagar.UseVisualStyleBackColor = true;
            this.btApagar.Click += new System.EventHandler(this.btApagar_Click);
            // 
            // desenhar
            // 
            this.desenhar.Location = new System.Drawing.Point(76, 24);
            this.desenhar.Name = "desenhar";
            this.desenhar.Size = new System.Drawing.Size(45, 23);
            this.desenhar.TabIndex = 0;
            this.desenhar.Text = "Colorir";
            this.desenhar.UseVisualStyleBackColor = true;
            this.desenhar.Click += new System.EventHandler(this.desenhar_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Colorir Pixel";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 143);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(56, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "Desenhar:";
            // 
            // checkBoxAlgoritmo
            // 
            this.checkBoxAlgoritmo.FormattingEnabled = true;
            this.checkBoxAlgoritmo.Items.AddRange(new object[] {
            "Reta (DDA)",
            "Reta (Bresenham)",
            "Circunferência (Bresenham)",
            "Mouse Livre"});
            this.checkBoxAlgoritmo.Location = new System.Drawing.Point(4, 159);
            this.checkBoxAlgoritmo.Name = "checkBoxAlgoritmo";
            this.checkBoxAlgoritmo.Size = new System.Drawing.Size(135, 64);
            this.checkBoxAlgoritmo.TabIndex = 8;
            this.checkBoxAlgoritmo.SelectedIndexChanged += new System.EventHandler(this.checkBoxAlgoritmo_SelectedIndexChanged);
            // 
            // btCor
            // 
            this.btCor.Location = new System.Drawing.Point(11, 61);
            this.btCor.Name = "btCor";
            this.btCor.Size = new System.Drawing.Size(108, 24);
            this.btCor.TabIndex = 5;
            this.btCor.Text = "Selecionar Cor";
            this.btCor.UseVisualStyleBackColor = true;
            this.btCor.Click += new System.EventHandler(this.btCor_Click);
            // 
            // txtY
            // 
            this.txtY.Location = new System.Drawing.Point(46, 26);
            this.txtY.Name = "txtY";
            this.txtY.Size = new System.Drawing.Size(24, 20);
            this.txtY.TabIndex = 4;
            this.txtY.Text = "y";
            // 
            // txtX
            // 
            this.txtX.Location = new System.Drawing.Point(12, 26);
            this.txtX.Name = "txtX";
            this.txtX.Size = new System.Drawing.Size(28, 20);
            this.txtX.TabIndex = 3;
            this.txtX.Text = "x";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(1, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(139, 13);
            this.label4.TabIndex = 13;
            this.label4.Text = "______________________";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(2, 122);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(139, 13);
            this.label10.TabIndex = 24;
            this.label10.Text = "______________________";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(1, 221);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(139, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "______________________";
            // 
            // tela
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1348, 697);
            this.Controls.Add(this.painel);
            this.Controls.Add(this.imagem);
            this.MinimumSize = new System.Drawing.Size(1364, 736);
            this.Name = "tela";
            this.Text = "SimplePaint - Trabalho de Computação Grafica";
            ((System.ComponentModel.ISupportInitialize)(this.imagem)).EndInit();
            this.painel.ResumeLayout(false);
            this.painel.PerformLayout();
            this.ResumeLayout(false);

        }

        private void Imagem_Paint( object sender, System.Windows.Forms.PaintEventArgs e ) {
            throw new System.NotImplementedException( );
        }

        #endregion

        private System.Windows.Forms.PictureBox imagem;
        private System.Windows.Forms.Panel painel;
        private System.Windows.Forms.TextBox txtY;
        private System.Windows.Forms.TextBox txtX;
        private System.Windows.Forms.Button desenhar;
        private System.Windows.Forms.Button btApagar;
        private System.Windows.Forms.Button btCor;
        private System.Windows.Forms.ColorDialog cdlg;
        private System.Windows.Forms.CheckedListBox checkBoxAlgoritmo;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckedListBox camadas_list;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox textBox5;
    }
}

