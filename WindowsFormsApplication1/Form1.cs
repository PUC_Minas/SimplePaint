﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

/// <summary>
/// Trabalho Pratico de Computação Grafica
/// 2º Semestre de 2016
/// Autor: Paulo Victor de Oliveira Leal
/// Matricula: 476169
/// </summary>
namespace SimplePaint {
    public partial class tela :Form {

        List<Desenho> images = new List<Desenho>( );

        Bitmap areaDesenho;
        Bitmap areaBackup;
        Color corPreenche;

        private Panel panel1;

        Point down = new Point( );
        Point up = new Point( );

        /// <summary>
        /// Construtor
        /// </summary>
        public tela( ) {
            InitializeComponent( );
            areaDesenho = new Bitmap( imagem.Size.Width, imagem.Size.Height );
            areaBackup = new Bitmap( imagem.Size.Width, imagem.Size.Height );
            corPreenche = Color.Black;
            this.panel1 = new System.Windows.Forms.Panel( );

            checkBoxAlgoritmo.CheckOnClick = true;
            checkBoxAlgoritmo.SelectionMode = SelectionMode.One;
            checkBoxAlgoritmo.SetItemCheckState( 0, CheckState.Checked );

            camadas_list.CheckOnClick = true;
            camadas_list.SelectionMode = SelectionMode.One;

        }

        /// <summary>
        /// Evento de segurar botão esquerdo mouse
        /// </summary>
        private void panel1_MouseDown( object sender, System.Windows.Forms.MouseEventArgs e ) {
            // Update the mouse path with the mouse information
            Point mouseDownLocation = new Point( e.X, e.Y );

            string eventString = null;
            switch( e.Button ) {
                case MouseButtons.Left:
                    Point a = mouseDownLocation;
                    imagem.Image = areaDesenho;
                    down = a;

                    panel1.Focus( );
                    panel1.Invalidate( );
                    eventString = "L";
                    break;
                case MouseButtons.None:
                default:
                    break;
            }
        }

        /// <summary>
        /// Evento de soltar botao esquerdo do mouse
        /// </summary>
        private void panel1_MouseUp( object sender, System.Windows.Forms.MouseEventArgs e ) {
            Point mouseUpLocation = new Point( e.X, e.Y );

            string eventString = null;
            switch( e.Button ) {
                case MouseButtons.Left:
                    Point b = mouseUpLocation;
                    up = b;

                    panel1.Focus( );
                    panel1.Invalidate( );
                    eventString = "L";
                    break;
                case MouseButtons.None:
                default:
                    break;
            }
            int pos = checkBoxAlgoritmo.SelectedIndex != -1 ? checkBoxAlgoritmo.SelectedIndex : 0;
            if( ( down.X < areaDesenho.Width && down.X > 0 ) && ( down.Y < areaDesenho.Height && down.Y > 0 ) &&
                  Math.Abs( down.X - up.X ) > 0 && Math.Abs( down.Y - up.Y ) > 0 ) {
                Bitmap temp = new Bitmap( Math.Abs( down.X - up.X ), Math.Abs( down.Y - up.Y ) );
                Desenho z = new Desenho( );

                if( pos == 0 ) {
                    z = new Desenho( ) {
                        bmap = AlgoritmoDeDDAParaReta( 0, 0, temp.Width, temp.Height, temp ),
                        down = down,
                        up = up,
                        algoritmo = true
                    };
                } else if( pos == 1 ) {
                    z = new Desenho( ) {
                        bmap = AlgoritmoDeBresenhamParaReta( 0, 0, temp.Width, temp.Height, temp ),
                        down = down,
                        up = up,
                        algoritmo = false
                    };
                } else if( pos == 2 ) {
                    AlgortitmoDeBresenhamParaCircunferencia( down.X, down.Y, up.X, up.Y, areaDesenho );
                    imagem.Image = areaDesenho;
                }

                if( pos == 0 || pos == 1 ) {
                    AdicionarNaLista( z );
                    if( camadas_list.Items.Count == 1 )
                        camadas_list.SetItemCheckState( 0, CheckState.Checked );
                    MergeImages( LocalizarCaso( z ) );
                }
            }
        }

        /// <summary>
        /// Efetua as comparações para decidir em qual caso a figura 
        /// se enquadra
        /// </summary>
        /// <param name="d">Objeto desenho</param>
        /// <returns></returns>
        private int LocalizarCaso( Desenho d ) {
            int caso = -1;
            if( d.up.X >= d.down.X && d.up.Y >= d.down.Y )
                caso = 0;
            else if( d.up.X <= d.down.X && d.up.Y >= d.down.Y )
                caso = 1;
            else if( d.up.X <= d.down.X && d.up.Y <= d.down.Y )
                caso = 2;
            else if( d.up.X >= d.down.X && d.up.Y <= d.down.Y )
                caso = 3;

            return caso;
        }

        /// <summary>
        /// Adciona na lista
        /// </summary>
        /// <param name="d">Desnho a ser adicionado</param>
        private void AdicionarNaLista( Desenho d ) {
            images.Add( d );
            camadas_list.Items.Add( d );
        }

        /// <summary>
        /// Remove da lista
        /// </summary>
        /// <param name="pos">Posicao a ser removida</param>
        private void RemoverDaLista( int pos ) {
            images.RemoveAt( pos );
            camadas_list.Items.RemoveAt( pos );
        }
        /// <summary>
        /// Chamado pelo botão para efetuar a translação
        /// de uma camada
        /// </summary>
        /// <param name="pos">Posição da camada</param>
        /// <param name="nx">Fator de translação para x</param>
        /// <param name="ny">Fator de translação para y</param>
        private void Translacao( int pos, int nx, int ny ) {
            Desenho d = images.ElementAt( pos );
            d.down = new Point( d.down.Y + nx, d.down.Y + ny );
            d.up = new Point( d.up.X + nx, d.up.Y + ny );
            using( Graphics g = Graphics.FromImage( areaDesenho ) ) {
                TratarDesenho( d, g );
            }
            Desenho x = new Desenho( ) {
                bmap = d.bmap.Clone( ) as Bitmap,
                setado = false,
                down = d.down,
                up = d.up,
                algoritmo = d.algoritmo
            };
            d.Dispose( );
            RemoverDaLista( pos );
            AdicionarNaLista( x );
            MergeImages( LocalizarCaso( x ) );
        }

        /// <summary>
        /// Chamado pelo botão para efetuar a rotação 
        /// de uma camada
        /// </summary>
        /// <param name="graus">Graus a rotacionar</param>
        /// <param name="pos">Posição da da camada</param>
        private void Rotacao( float graus, int pos ) {
            Desenho d = images.ElementAt( pos );
//            d.down = new Point( (int)( Math.Cos( graus ) * d.down.X ) - ((int) Math.Sin( graus ) * d.down.Y ), 
//                                (int)( Math.Cos( graus ) * d.down.Y ) + ( (int)Math.Sin( graus ) * d.down.X ) );
            d.up = new Point(   
                (int)( Math.Cos( graus ) * d.up.X ) - ( (int)Math.Sin( graus ) * d.up.Y ),
                (int)( Math.Cos( graus ) * d.up.Y ) + ( (int)Math.Sin( graus ) * d.up.X ) 
            );              


            Desenho x = new Desenho( ) {
                bmap = (Bitmap) d.bmap.Clone( ),
                setado = false,
                down = d.down,
                up = d.up,
                algoritmo = d.algoritmo
            };
            RemoverDaLista( pos );
            AdicionarNaLista( x );
            MergeImages( LocalizarCaso( x ) );
        }

        /// <summary>
        /// Chamado pelo botão para aplicar a escala
        /// em uma camada
        /// </summary>
        /// <param name="ex">Escala para x</param>
        /// <param name="ey">Escalada para y</param>
        /// <param name="pos">Posicao da camada</param>
        private void Escala( int ex, int ey, int pos ) {
            Desenho d = images.ElementAt( pos );
            
            int x3 = d.bmap.Width * ex;
            int y3 = d.bmap.Height * ey;

            Bitmap temp = new Bitmap( x3, y3 );
            RemoverDaLista( pos );
            Desenho z = new Desenho( );
            z.bmap = d.algoritmo ? AlgoritmoDeDDAParaReta( 0, 0, temp.Width, temp.Height, temp ) : AlgoritmoDeBresenhamParaReta( 0, 0, temp.Width, temp.Height, temp );
            z.down = d.down;
            z.up = d.up;
            z.algoritmo = d.algoritmo;
            AdicionarNaLista( z );
            MergeImages( LocalizarCaso( z ) );
        }

        /// <summary>
        /// De acordo com o caso calculado desenha devidamente
        /// a figura
        /// </summary>
        /// <param name="d">Objeto de desenho a ser desenhado</param>
        /// <param name="g">Graphics</param>
        private void TratarDesenho( Desenho d, Graphics g ) {
            int caso = LocalizarCaso( d );
            if( caso == 0 ) {
                g.DrawImage( d.bmap, new Rectangle( d.down.X, d.down.Y, d.bmap.Width, d.bmap.Height ) );
            } else if( caso == 1 ) {
                g.DrawImage( d.bmap, new Rectangle( d.down.X, d.down.Y, -d.bmap.Width, d.bmap.Height ) );
            } else if( caso == 2 ) {
                g.DrawImage( d.bmap, new Rectangle( d.down.X, d.down.Y, -d.bmap.Width, -d.bmap.Height ) );
            } else {
                g.DrawImage( d.bmap, new Rectangle( d.down.X, d.down.Y, d.bmap.Width, -d.bmap.Height ) );
            }
        }

        /// <summary>
        /// Junta todas as camadas na area de desenho
        /// </summary>
        /// <param name="caso">Codigo do caso calculado</param>
        private void MergeImages( int caso ) {
            areaDesenho = (Bitmap)areaBackup.Clone( );
            using( Graphics g = Graphics.FromImage( areaDesenho ) ) {
                int offset = 0;
                foreach( Desenho image in images ) {
                    TratarDesenho( image, g );
                    image.setado = true;
                    Debug.WriteLine( "DESENHADO" );
                }
            }
            imagem.Image = areaDesenho;
        }

        /// <summary>
        /// Desenhar com base nos (x, y) informados
        /// </summary>
        private void desenhar_Click( object sender, EventArgs e ) {
            int x = (int)Convert.ToInt64( txtX.Text );
            int y = (int)Convert.ToInt64( txtY.Text );

            Colore( x, y, areaDesenho );
        }

        /// <summary>
        /// Abre a caixa de cores para escolher
        /// </summary>
        private void btCor_Click( object sender, EventArgs e ) {
            DialogResult result = cdlg.ShowDialog( );
            if( result == DialogResult.OK ) {
                corPreenche = cdlg.Color;
            }
        }

        /// <summary>
        /// Limpa toda a area de desenho
        /// </summary>
        private void btApagar_Click( object sender, EventArgs e ) {
            areaDesenho = new Bitmap( imagem.Size.Width, imagem.Size.Height );
            areaBackup = new Bitmap( imagem.Size.Width, imagem.Size.Height );
            images.Clear( );
            camadas_list.Items.Clear( );
            imagem.Image = areaDesenho;
        }

        /// <summary>
        /// Desenha com base no movimento do mouse livre
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imagem_MouseMove( object sender, MouseEventArgs e ) {
            int pos = checkBoxAlgoritmo.SelectedIndex != -1 ? checkBoxAlgoritmo.SelectedIndex : 0;

            if( e.Button == MouseButtons.Left && pos == 3 ) {
                int x = e.X;
                int y = e.Y;

                txtX.Text = Convert.ToString( x );
                txtY.Text = Convert.ToString( y );

                Colore( x, y, areaDesenho );
                imagem.Image = areaDesenho;
            }
        }

        /// <summary>
        /// Colore de fato o pixel
        /// </summary>
        /// <param name="x">Coordenada x</param>
        /// <param name="y">Coordenada y</param>
        /// <param name="bmap">Bitmap a ser colorido</param>
        private void Colore( int x, int y, Bitmap bmap ) {
            Debug.WriteLine( x + "," + y );
            if( ( x < bmap.Width && x > 0 ) && ( y < bmap.Height && y > 0 ) ) {
                bmap.SetPixel( x, y, corPreenche );
                Debug.WriteLine( "Pixel setado em:" + x + "," + y );
            }
        }

        public Bitmap AlgoritmoDeDDAParaReta( float x1, float y1, float x2, float y2, Bitmap bmap ) {
            float dx = x2 - x1;
            float dy = y2 - y1;
            float x = x1;
            float y = y1;
            float passos;
            Colore( (int)x, (int)y, bmap );
            if( Math.Abs( dx ) > Math.Abs( dy ) ) {
                passos = Math.Abs( dx );
            } else {
                passos = Math.Abs( dy );
            }
            float xincr = dx / passos;
            float yincr = dy / passos;

            for( int i = 1; i <= passos; i++ ) {
                x += xincr;
                y += yincr;
                Colore( (int)Math.Round( x ), (int)Math.Round( y ), bmap );
            }
            return bmap;
        }

        /// <summary>
        /// Algoritmo Bresenham Retas
        /// </summary>
        /// <param name="iniX">Coordenada X Inicial</param>
        /// <param name="iniY">Coordenada Y Inicial</param>
        /// <param name="fimX">Coordenada X Final</param>
        /// <param name="fimY">Coordenada Y Final</param>
        private Bitmap AlgoritmoDeBresenhamParaReta( int x1, int y1, int x2, int y2, Bitmap bmap ) {
            int x, y, xincr, yincr, p;
            int dx = x2 - x1;
            int dy = y2 - y1;
            if( dx < 0 ) {
                xincr = -1;
                dx = -dx;
            } else {
                xincr = 1;
            }
            if( dy < 0 ) {
                yincr = -1;
                dy = -dy;
            } else {
                yincr = 1;
            }

            x = x1;
            y = y1;

            Colore( x, y, bmap );

            if( dx > dy ) { // 1º caso
                p = 2 * dy - dx;
                for( int i = 0; i < dx; i++ ) {
                    x += xincr;
                    if( p < 0 ) {
                        p += 2 * dy;
                    } else {
                        y += yincr;
                        p += 2 * ( dy - dx );
                    }
                    Colore( x, y, bmap );
                }
            } else {
                p = 2 * dx - dy;
                for( int i = 0; i < dy; i++ ) {
                    y += yincr;
                    if( p < 0 ) {
                        p += 2 * dx;
                    } else {
                        x += xincr;
                        p += 2 * ( dx - dy );
                    }
                    Colore( x, y, bmap );
                }
            }
            return bmap;
        }

        /// <summary>
        /// Aplica o algoritmo de Bresenham para circunferência
        /// </summary>
        /// <param name="x1">Coordenada inicial de x</param>
        /// <param name="y1">Coordenada inicial de y</param>
        /// <param name="x2">Coordenada final de x</param>
        /// <param name="y2">Coordenada final de y</param>
        /// <param name="bmap">Bitmap a ser desenhado</param>
        /// <returns></returns>
        public Bitmap AlgortitmoDeBresenhamParaCircunferencia( int x1, int y1, int x2, int y2, Bitmap bmap ) {
            int raio = (int)Math.Sqrt( Math.Pow( ( x2 - x1 ), 2 ) + ( Math.Pow( ( y2 - y1 ), 2 ) ) );
            Debug.WriteLine( "RAIO = " + raio );
            int x = 0;
            int y = raio;
            ColoreSimetricos( x, y, x1, y1, bmap );
            int p = 3 - 2 * raio;
            while( x < y ) {
                if( p < 0 )
                    p += ( 4 * x ) + 6;
                else {
                    p += 4 * ( x - y ) + 10;
                    y--;
                }
                x++;

                ColoreSimetricos( x, y, x1, y1, bmap );
            }
            return bmap;
        }

        /// <summary>
        /// Chamado pela circunferencia para colorir os pixels simetricos
        /// </summary>
        public void ColoreSimetricos( int a, int b, float xc, float yc, Bitmap bmap ) {
            Colore( (int)xc + a, (int)yc + b, bmap );
            Colore( (int)xc + a, (int)yc - b, bmap );
            Colore( (int)xc - a, (int)yc + b, bmap );
            Colore( (int)xc - a, (int)yc - b, bmap );
            Colore( (int)xc + b, (int)yc + a, bmap );
            Colore( (int)xc + b, (int)yc - a, bmap );
            Colore( (int)xc - b, (int)yc + a, bmap );
            Colore( (int)xc - b, (int)yc - a, bmap );
        }

        /// <summary>
        /// Função para permitir selecionar somente um check box
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void checkBoxAlgoritmo_SelectedIndexChanged( object sender, EventArgs e ) {
            int iSelectedIndex = checkBoxAlgoritmo.SelectedIndex;
            if( iSelectedIndex == -1 )
                return;
            for( int iIndex = 0; iIndex < checkBoxAlgoritmo.Items.Count; iIndex++ )
                checkBoxAlgoritmo.SetItemCheckState( iIndex, CheckState.Unchecked );
            checkBoxAlgoritmo.SetItemCheckState( iSelectedIndex, CheckState.Checked );
        }
        
        /// <summary>
        /// Permite selecionar somente uma camada nos checkbo
        /// </summary>
        private void camadas_list_SelectedIndexChanged( object sender, EventArgs e ) {
            int iSelectedIndex = camadas_list.SelectedIndex;
            if( iSelectedIndex == -1 )
                return;
            for( int iIndex = 0; iIndex < camadas_list.Items.Count; iIndex++ )
                camadas_list.SetItemCheckState( iIndex, CheckState.Unchecked );
            camadas_list.SetItemCheckState( iSelectedIndex, CheckState.Checked );
        }

        /// <summary>
        /// Botão de chamada para translação
        /// </summary>
        private void button2_Click_1( object sender, EventArgs e ) {
            int x = (int)Convert.ToInt64( textBox2.Text );
            int y = (int)Convert.ToInt64( textBox1.Text );
            int pos = camadas_list.SelectedIndex != -1 ? camadas_list.SelectedIndex : 0;
            Translacao( pos, x, y );
        }


        /// <summary>
        /// Botão de chamada para Rotação
        /// </summary>
        private void button3_Click_1( object sender, EventArgs e ) {
            int x = (int)Convert.ToInt64( textBox4.Text );
            int pos = camadas_list.SelectedIndex != -1 ? camadas_list.SelectedIndex : 0;
            Rotacao( x, pos );
        }

        /// <summary>
        /// Botão de chamada para Escala
        /// </summary>
        private void button1_Click( object sender, EventArgs e ) {
            int x = (int)Convert.ToInt64( textBox3.Text );
            int y = (int)Convert.ToInt64( textBox5.Text );
            int pos = camadas_list.SelectedIndex != -1 ? camadas_list.SelectedIndex : 0;
            Escala( x, y, pos );
        }
    }
}